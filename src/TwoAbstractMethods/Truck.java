package TwoAbstractMethods;

public class Truck extends Vehicle{
	public Truck(String string){
		super(string);
	}
	
	@Override
	public String PlayCD() {  
        return"CD is not playing";
	}
}