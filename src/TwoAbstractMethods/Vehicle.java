package TwoAbstractMethods;

public abstract class Vehicle {
	
	public String num_Wheels;
	
	
	//super class ੾�� default constructor	
	public Vehicle() {
	}
	
	public Vehicle(String num_Wheels) {
		this.num_Wheels = num_Wheels;
	}
	
	public abstract String PlayCD();
	
	public String toString(){
		return this.num_Wheels;
	}
}