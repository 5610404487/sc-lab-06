package TwoAbstractMethods;

public class Car extends Vehicle{
	public Car(String num_Wheels){
		super(num_Wheels);
	}
	
	@Override
	public String PlayCD() {  
        return"CD is playing"; 
	}
}