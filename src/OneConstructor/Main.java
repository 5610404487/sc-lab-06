package OneConstructor;

import java.util.ArrayList;
public class Main {
	
	public static void main (String[] args){
		//Vehicle v1 = new Vehicle();
		//System.out.println(v1);
		Car c1 = new Car();
		Car c2 = new Car("4");
		System.out.println(c1);
		System.out.println(c2);
		Truck t1 = new Truck();
		Truck t2 = new Truck("6");
		System.out.println(t1);
		System.out.println(t2);
	}
}