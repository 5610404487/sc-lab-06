package ThreeVariableMethod;

public class Vehicle {
	
//	3.1
//	private String car = "TOYOTA";
//	public String Vehicle(){
//		String car = "Honda";
//		return car;
//	}
//	public String toString(){
//		return car;
//	}
	
//	3.2
	private String car = "TOYOTA"; 
	public String Vehicle(){
		int count = 0;
//		count++;
		if(count==1){
			String car = "Honda";
			System.out.println("Honda");
		}
		else{
			int car = 0;
			System.out.println(0);
		}
		return car;
	}
	public String toString(){
		return car;
	}
}
